package user;

import java.time.LocalDate;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.Module;
import model.Personne;
import model.TypePersonne;
import repo.FiliereRepository;
import repo.ModuleRepository;
import repo.PersonneRepository;

public class TestNassim {

	public static void main(String[] args) {
		testRCreateModule();
	}

	static void testRfindAll() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		PersonneRepository personneRepository = ctx.getBean(PersonneRepository.class);
		System.out.println(personneRepository.findAll());
		ctx.close();
	}
	
static void testRCreatePersonne() {
		
//		Personne p =new Personne("Einstein", "Albert", TypePersonne.FORMATEUR);
		
		Personne p =new Personne("Curie", "Marie", TypePersonne.STAGIAIRE);
		
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        PersonneRepository personneRepository = ctx.getBean(PersonneRepository.class);
        ClassPathXmlApplicationContext ctx2 = new ClassPathXmlApplicationContext("applicationContext.xml");
        FiliereRepository fr = ctx2.getBean(FiliereRepository.class);
        
        p.setFiliere(fr.findById(100).get());
        personneRepository.save(p);
        
        ctx.close();
        ctx2.close();
    }
	
	static void testRCreateModule() {
		
		//Personne p =new Personne("Einstein", "Albert", TypePersonne.FORMATEUR);
		Module m = new Module("COBOL C--",LocalDate.of(1981, 2, 24) , LocalDate.of(1991, 2, 25));
		
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        PersonneRepository personneRepository = ctx.getBean(PersonneRepository.class);
        ClassPathXmlApplicationContext ctx2 = new ClassPathXmlApplicationContext("applicationContext.xml");
        FiliereRepository fr = ctx2.getBean(FiliereRepository.class);
        ClassPathXmlApplicationContext ctx3 = new ClassPathXmlApplicationContext("applicationContext.xml");
        ModuleRepository mr = ctx3.getBean(ModuleRepository.class);
        
        m.setFiliere(fr.findById(100).get());
        m.setFormateur(personneRepository.findById(100).get());
        mr.save(m);
        
        ctx.close();
        ctx2.close();
        ctx3.close();
    }
	
}
