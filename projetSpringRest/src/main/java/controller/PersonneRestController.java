package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Personne;
import repo.PersonneRepository;

@RestController
@RequestMapping("/personnes")
public class PersonneRestController {
	
	@Autowired PersonneRepository repo;
	
	@GetMapping("/test")
	public String test(){
		return "test ok";
	}
	
	//GET Mapping= SELECT
	//POST Mapping = CREATE
	//PUT Mapping= UPDATE
	//DELETE Mapping= SUPPRESSION
	
	@GetMapping("/personne")
	public List<Personne> findall(){
		return repo.findAll();
	}
	
	@GetMapping("/personne/{id}")
	public Personne findbyid(@PathVariable(name="id") int id){
		return repo.findById(id).get();
	}
	
	@GetMapping("/personne/nom/{container}")
	public List<Personne> findbynomcontainer(@PathVariable(name="container") String container){
		return repo.findByNomContaining(container);
	}
	
	@PostMapping("/personne")
	public void create(@RequestBody Personne p){
		repo.save(p);
	}
	
	@PutMapping("/personne")
	public String update(@RequestBody Personne p){
		repo.save(p);
		return "Personne "+ p.getId() + " mise � jour";
	}
	
	@DeleteMapping("/personne/{id}")
	public String delete(@PathVariable(name="id") int id){
		repo.deleteById(id);
		return "Personne "+ id + " supprim�e";
	}
	
	
}
