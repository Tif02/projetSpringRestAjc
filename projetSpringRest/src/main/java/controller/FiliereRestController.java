package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Filiere;
import model.Module;
import repo.FiliereRepository;

@RestController
@RequestMapping("/filieres")
public class FiliereRestController {
	
	@Autowired
	FiliereRepository repo;
	
	@GetMapping("/test")
	public String test(){
		return "TOUT les SYSTEMES OK !!!!!!";
	}
	
	@GetMapping("/filiere")
	public List<Filiere> findall(){
		return repo.findAll();
	}
	@GetMapping("/filiere/{id}")
	public Filiere findbyid(@PathVariable(name = "id") int id){
		return repo.findById(id).get();
	}
	
	@GetMapping("/filiere/modules/{id}")
	public List<Module> findmodule(@PathVariable(name = "id") int id){
		return repo.findById(id).get().getModules();
	}
	
	@PostMapping("/filiere")
	public void create(@RequestBody Filiere f){
		repo.save(f);
	}
	@PutMapping("/filiere")
	public String update(@RequestBody Filiere f){
		repo.save(f);
		return "Filiere "+ f.getId() + " mise � jour";
	}	
	@DeleteMapping("/filiere/{id}")
	public String delete(@PathVariable(name="id") int id){
		repo.deleteById(id);
		return "Filiere "+ id + " supprim�e";
	}

}
