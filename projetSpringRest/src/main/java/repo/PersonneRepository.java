package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Integer>{
	
	public List<Personne> findByNom(String nom);
	public List<Personne> findByNomContaining(String nom);

}