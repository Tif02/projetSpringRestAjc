package repo;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Integer>{

}
