package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "personne_gen", sequenceName = "personne_seq", initialValue = 100, allocationSize = 1)
public class Personne {

	@Id
	@GeneratedValue(generator = "personne_gen")
	private Integer id;

	private String nom;

	private String prenom;

	
	private TypePersonne type;

	@ManyToOne
	@JoinColumn
	//@JsonIgnoreProperties("stagiaire")
	@JsonIgnore
	private Filiere filiere;
	
	@OneToMany(mappedBy="formateur")
	@JsonIgnore
	private List<Module> modules;

	public Personne(String nom, String prenom, TypePersonne type) {
		this.nom = nom;
		this.prenom = prenom;
		this.type = type;
	}

	public Personne() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Enumerated(EnumType.STRING)
	public TypePersonne getType() {
		return type;
	}

	public void setType(TypePersonne type) {
		this.type = type;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", type=" + type + "]";
	}
	
	
	

}
