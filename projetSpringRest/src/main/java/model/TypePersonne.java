package model;


public enum TypePersonne {
	STAGIAIRE("stagiaire"), FORMATEUR("formateur");

	private String libelle;

	TypePersonne(String libelle) {
		this.libelle = libelle;
	}

	public String toString() {
		return this.libelle;
	}

	public static TypePersonne fromString(String libelle) {
		for (TypePersonne tp : values()) {
			if (tp.libelle.equals(libelle))
				return tp;
		}
		return null;
	}
	

}
