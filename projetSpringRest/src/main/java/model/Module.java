package model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "module_gen", sequenceName = "module_seq", initialValue = 100, allocationSize = 1)
public class Module {

	@Id
	@GeneratedValue(generator = "module_gen")
	private Integer id;

	private String libelle;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	@ManyToOne
	@JoinColumn
	//@JsonIgnoreProperties("modules") // RAJOUUUUUUUUUUUUUUUUUUT !
	@JsonIgnore
	private Personne formateur;
	
	@ManyToOne
	@JoinColumn
	//@JsonIgnoreProperties("modules")
	@JsonIgnore
	private Filiere filiere;

	public Module() {
	}

	public Module(String libelle, LocalDate dateDebut, LocalDate dateFin) {
		this.libelle = libelle;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public Personne getFormateur() {
		return formateur;
	}

	public void setFormateur(Personne formateur) {
		this.formateur = formateur;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	@Override
	public String toString() {
		return "Module [libelle=" + libelle + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
	}
	
	

}
